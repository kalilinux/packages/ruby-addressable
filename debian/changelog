ruby-addressable (2.3.6-1kali1) kali; urgency=low

  * Import from Debian.
  * Drop ruby-coveralls from Build-Depends as it's not available in Kali
    and is only required for some tests that are skipped when unavailable.
  * Ignore failing tests on ruby1.8

 -- Sophie Brun <sophie@freexian.com>  Thu, 22 May 2014 13:37:03 +0000

ruby-addressable (2.3.6-1) unstable; urgency=medium

  * Imported Upstream version 2.3.6
  * Refresh remove_lib_from_loadpath.patch

 -- Cédric Boutillier <boutil@debian.org>  Wed, 07 May 2014 23:51:51 +0200

ruby-addressable (2.3.5-1) unstable; urgency=low

  * Team upload.
  * New upstream release. (Closes: #740794).
  * Bump Standards-Version to 3.9.5. No changes were required.
  * Refresh patches.
  * Add Build-Depends on ruby-coveralls.

 -- Miguel Landaeta <nomadium@debian.org>  Tue, 04 Mar 2014 18:10:58 -0300

ruby-addressable (2.3.4-1) unstable; urgency=low

  [ Christian Hofstaedtler ]
  * Install CHANGELOG.md as upstream changelog

  [ Cédric Boutillier ]
  * New upstream version
  * debian/control:
    + use canonical URI in Vcs-* fields
    + bump Standards-Version to 3.9.4 (no changes needed)
    + add myself to Uploaders
    + drop libaddressable-ruby* transitional packages
    + depend on ruby instead of ruby1.8
  * move unicode.data to /usr/share/ruby-addressable
    + debian/rules: install file where needed
    + debian/patches/move_unicode_data.patch:
        fix path of unicode.data in the library
    + debian/patches/remove_lib_from_loadpath.patch:
        remove lib from the load path in the spec using unicode.data 
        to use the version of the library installed in debian/
  * remove lintian overrides, about transitional packages

 -- Cédric Boutillier <boutil@debian.org>  Tue, 04 Jun 2013 23:03:28 +0200

ruby-addressable (2.2.8-1) unstable; urgency=low

  * Team upload
  * New upstream version
  * Enable test suite. Build-depend on ruby-rspec, rake.
  * Build against gem2deb >= 0.3.0
  * Bump Standards-Version to 3.9.3 (no changes needed)
  * Set priority of transitional packages to extra
  * Improve a little bit the short description
  * Convert debian/copyright to DEP-5 copyright-format/1.0
  * Override lintian warning about duplicate description of transitional
    packages
  * Replace Conflicts: with Breaks: for dependency relations on old packages
  * remove website/index.html from installed docs (not informative)

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Fri, 29 Jun 2012 21:30:47 +0200

ruby-addressable (2.2.6-1) unstable; urgency=low

   * New upstream release
   * Switch to gem2deb-based packaging. Rename source and binary package.
   * debian/control
       - Removed DM upload tag.
       - Bumped the standard version.
   * debian/copyright
       - update copyright information.
  
 -- Deepak Tripathi <deepak@debian.org>  Tue, 31 May 2011 14:51:45 +0530
 
libaddressable-ruby (2.2.2-1) unstable; urgency=low

   * Initial release (Closes: #594294)
   
 -- Deepak Tripathi <deepak@debian.org>  Wed, 13 Oct 2010 22:13:02 +0530
